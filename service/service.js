const { cars } = require('../models')
const storage = require('node-sessionstorage');
const { name } = require('ejs');
const { response } = require('express');


const getallData = async (req, res) => {
    const carList = await cars.findAll();
    let newcar = JSON.stringify(carList, null, 2);
      newcar = JSON.parse(newcar);
      console.log(newcar);
  
      res.render("partial/list", {
        layout: "index",
        title: "List Car",
        cars: newcar
      });
  
    };
  
  const getDatabyID = async (id) => {
    const carbyID = await cars.findOne({
      where: { id:id },
    });
    let newcar = JSON.stringify(carbyID, null, 2);
    newcar = JSON.parse(newcar);
    return newcar;
  }
    
  const pageAdd = (req, res) => {
    res.render("partial/add", {
      layout: "index",
      title: "Add New Data Car",
    });
  }
  
  const pageUpdate = (req, res) => {
    const car = getDatabyID(req.params.id);
    car.then(function (result){
      res.render("partial/update", {
      layout: "index",
      title: "Update Data Car",
      car: result,
    });
  });
  }

  const pageDelete = (req, res) => {
    const car = getDatabyID(req.params.id)
    car.then(function (result){
      res.render("partial/delete", {
      layout: "index",
      title: "Delete Data Car",
      car: result,
    });
  });
  }

  const updateCars = (req,res) => {
    let newType = "";
    if(req.body.type == "Tipe Ukuran") {
      newType = req.body.beforeType;
    }  else {
      newType = req.body.type;
    }

    const query = {
      where: { 
        id: req.body.id }
    };

    cars.update(
      {
      name: req.body.name,
      type: req.body.type,
      sewa: req.body.sewa,
      img: req.body.img,
    }, 
    query
    ).catch((err) => {
      console.log(err);
    });
    res.redirect("/");
  }

  const createCar = (data) => {
    cars
    .create({
      name: data.name,
      type: data.type,
      sewa: data.sewa,
      img: data.img
    })
    .then((cars) => {
      console.log(cars);
    })
    .catch((err) => {
      console.log(err)
      response.status(500).send("Something went wrong")
    });
  }

  const addCar = (req,res) => {
    createCar(req.body);
    setTimeout(changerDir, 2000)
  }

  const deleteData = (req, res) => {
    cars.destroy({
      where: {
        id: req.params.id,
      },
    }).then(() => {
           res.status(200).redirect("/");
    })
    .catch((err) => {
      console.log(err)
      response.status(500).send("Something went wrong")
    });
  }

  const filter = async (req, res) => {
    const carList = await cars.findAll();
    let newcar = JSON.stringify(carList, null, 2);
    newcar = JSON.parse(newcar);
  
    const car = newcar.filter((row) => row.type == req.params.type);
    console.log(car)

    res.render("partial/list", {
      layout: "index",
      title: "List Car",
      cars: car
    });
  };

  const search = async (req, res) => {
    const sequelize = require("sequelize");
    const Op = sequelize.Op;
    const likename = "%"+ req.body.searchCar + "%"
    const carList = await cars.findAll({
      where: {
        name:  { [Op.like]: `%${likename}%` }
      },
    });
    let searchCar = JSON.stringify(carList, null, 2);
    searchCar = JSON.parse(searchCar);
  
    res.render("partial/list", {
      layout: "index",
      title: "List Car",
      cars: searchCar
    });
  };  
  
  module.exports = {
    getallData,
    getDatabyID,
    pageAdd,
    pageUpdate,
    updateCars,
    pageDelete,
    createCar,
    addCar,
    deleteData,
    filter,
    search
  }
  