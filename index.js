const express = require("express");
const expresslayouts = require("express-ejs-layouts");
const bp = require("body-parser");
const carPage = require("./service/service");

const app = express();
const port = 3000;

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }));

// view engine with ejs
app.set("view engine", "ejs");

// using expresslayout for frontend
app.use(expresslayouts);

// use public domain as a static
app.use(express.static("public"));

//view index carlist
app.get("/", carPage.getallData)

//view add new car page and add new car
app.get("/add", carPage.pageAdd)
app.post("/add", carPage.addCar)

//view update car and update function
app.get("/update/:id", carPage.pageUpdate)
app.post("/updatedata/:id", carPage.updateCars)

//delete function
app.get("/delete/:id", carPage.pageDelete)
app.post("/deletedata/:id", carPage.deleteData)

//filter and search function
app.get("/filter/:type", carPage.filter);
app.post("/searchCar", carPage.search)


app.listen(port, function () {
    console.log(`Server started on port ${port}...`)
} )
